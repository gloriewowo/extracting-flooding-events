import sys
import os
import pandas as pd
import geopandas as gpd
import numpy as np
from plotnine import *
import plotly
import seaborn as sns
import matplotlib as mpl
from matplotlib import gridspec
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as mcolors
from matplotlib import ticker

from datetime import datetime
from datetime import date

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
PROJECT_ROOT = BASE_DIR + "/"
os.chdir(PROJECT_ROOT)


#  REORGANIZE DATA ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ REORGANIZE DATA
# sys.path.append("/Users/Allegra/Documents/Postdoc/habitus/modules/data_in_use/")

path = PROJECT_ROOT
output = path + "data_in_use/" + "n_values.csv"

# shape_path = "/Users/Allegra/Documents/Postdoc/habitus/modules/github/SRVzones/shapes/"


p19 = pd.read_csv(path + "N-data/N_time_series_2019.csv")

p20 = pd.read_csv(path + "N-data/N_time_series_2020.csv")

p21 = pd.read_csv(path + "N-data/N_time_series_2021.csv")

p22 = pd.read_csv(path + "N-data/N_time_series_2022.csv")

pcol19 = p19.filter(regex=("\d{4}-?\d{2}-?\d{2}$"))
pcol19[["ID", "geometry", "Latitude", "Longitude", "aoi_area"]] = p19[
    ["ID", "geometry", "Latitude", "Longitude", "aoi_area"]
]
pcol19 = pcol19.melt(
    id_vars=["ID", "geometry", "Latitude", "Longitude", "aoi_area"],
    var_name="date",
    value_name="n",
)
pcol19["year"] = 2019

pcol20 = p20.filter(regex=("\d{4}-?\d{2}-?\d{2}$"))
pcol20[["ID", "geometry", "Latitude", "Longitude", "aoi_area"]] = p20[
    ["ID", "geometry", "Latitude", "Longitude", "aoi_area"]
]
pcol20 = pcol20.melt(
    id_vars=["ID", "geometry", "Latitude", "Longitude", "aoi_area"],
    var_name="date",
    value_name="n",
)
pcol20["year"] = 2020

pcol21 = p21.filter(regex=("\d{4}-?\d{2}-?\d{2}$"))
pcol21[["ID", "geometry", "Latitude", "Longitude", "aoi_area"]] = p21[
    ["ID", "geometry", "Latitude", "Longitude", "aoi_area"]
]
pcol21 = pcol21.melt(
    id_vars=["ID", "geometry", "Latitude", "Longitude", "aoi_area"],
    var_name="date",
    value_name="n",
)
pcol21["year"] = 2021

pcol22 = p22.filter(regex=("\d{4}-?\d{2}-?\d{2}$"))
pcol22[["ID", "geometry", "Latitude", "Longitude", "aoi_area"]] = p22[
    ["ID", "geometry", "Latitude", "Longitude", "aoi_area"]
]
pcol22 = pcol22.melt(
    id_vars=["ID", "geometry", "Latitude", "Longitude", "aoi_area"],
    var_name="date",
    value_name="n",
)
pcol22["year"] = 2022

pcol = pd.concat([pcol19, pcol20, pcol21, pcol22])

print(pcol.head())
#  WRITE CSV ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ WRITE CSV
pcol.to_csv(output)
